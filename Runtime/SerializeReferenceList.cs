﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SixParQuatre
{
    [System.Serializable]
    public class SerializeReferenceList<T>: IEnumerable
    {
        [SerializeReference]
        public List<T> List = new List<T>();
        public int Count => List.Count;
        public void Add(T element) => List.Add(element);
        public bool Remove(T element) => List.Remove(element);
        public void RemoveAt(int i) => List.RemoveAt(i);
        public void Clear() => List.Clear();
        
        public IEnumerator GetEnumerator() => List.GetEnumerator();

        public T this[int i]
        {
            get { return this.List[i]; }
            set { this.List[i] = value; }
        }
    }
}
