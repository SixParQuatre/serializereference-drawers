Description of package changes in reverse chronological order. It is good practice to use a standard format, like [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
# Changelog

## [1.0.0-pre.2] - 2022-12-4

### Changed
- Readme and documentationURL

## [1.0.0-preview] - 2022-11-30

### Added
- Users can now use [ClassPicker] to get the dropdown for existing SerializedReference variable without having to create the new drawer classes. Downside is that this won't show the dropdown control next the the +/- on lists. 