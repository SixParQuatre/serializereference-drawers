using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace SixParQuatre
{
    /// <summary>
    /// The base PropertyDrawer for a property that's marked SerializeReference
    /// </summary>
    [CustomPropertyDrawer(typeof(ClassPickerAttribute))]
    public class SerializeReferenceDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (IsAssigned(property))
                return EditorGUI.GetPropertyHeight(property, true);
            else
                return EditorGUIUtility.singleLineHeight;
        }
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (position.width < 1)
                return;

            bool isAssigned = IsAssigned(property);
            if (!SerializeReferenceUtils.IsInArray(property))
            {
                // the type might be Generic for a subclass with no child classes of its own.
                bool isAssignable = property.propertyType == SerializedPropertyType.ManagedReference;

                //For a single field
                //1. if it is assigned, display the clear button and return if it is clicked
                if (isAssigned)
                {
                    if (SerializeReferenceUtils.ClearPropertyButton(ref position, property))
                        return;
                }

                //2. if it is assigned or not assignable (i.e. the field type doesn't have child classes), call the OnGUI_Specific
                if (isAssigned || !isAssignable)
                {
                    OnGUI_Specific(position, property, label);
                }
                else
                {
                    //... otherwise, show the field label and the Select menu
                    position = EditorGUI.PrefixLabel(position, label);
                    SerializeReferenceUtils.PopupType(position, property, fieldInfo.FieldType);
                }

            }

            else //This property is part of an array
            {
                if (isAssigned)
                {
#if !UNITY_2020_1_OR_NEWER
                if (SerializeReferenceUtils.ClearPropertyButton(ref position, property))
                    return;
#endif
                    OnGUI_Specific(position, property, label);
                }
                else
                {
                    System.Type listType = SerializeReferenceListDrawer.CurrentListType;
                    if (listType == null)
                    {
                        string typeName = property.managedReferenceFieldTypename;
                        int splitIndex = typeName.IndexOf(' ');
                        var assembly = Assembly.Load(typeName.Substring(0, splitIndex));
                        listType = assembly.GetType(typeName.Substring(splitIndex + 1));
                        
                    }
                    SerializeReferenceUtils.PopupType(position, property, listType);
                }
            }
        }

        protected Rect OnGUI_FirstLine(Rect position, SerializedProperty property, GUIContent label, bool useFoldOut = true)
        {
            //By default, display the property label
            Rect posLabel = new Rect(position);
            posLabel.height = EditorGUIUtility.singleLineHeight;

            if (useFoldOut)
            {
                property.isExpanded = EditorGUI.Foldout(posLabel, property.isExpanded, label);
                float width = EditorStyles.foldout.CalcSize(label).x;
                posLabel.x += width;
                posLabel.width -= width;
            }
            else
                posLabel = EditorGUI.PrefixLabel(posLabel, label);
            if (IsAssigned(property))
            {
                string typeName = property.managedReferenceFullTypename;
                int startIndex = Mathf.Max(typeName.IndexOf(" "), typeName.LastIndexOf(".")) + 1;
                GUIContent ct = new GUIContent("<i>" + typeName.Substring(startIndex) + "</i>");
                if (richLabel == null)
                {
                    richLabel = new GUIStyle(EditorStyles.label);
                    richLabel.richText = true;
                }

                float width = richLabel.CalcSize(ct).x;
                posLabel.x += posLabel.width - width;
                posLabel.width = width;
                EditorGUI.LabelField(posLabel, ct, richLabel);
            }

            //give back the full width on the next line
            if (!SerializeReferenceUtils.IsInArray(property))
                position.width += SerializeReferenceUtils.clearButtonWidth;
            return position;

        }

        GUIStyle richLabel = null;
        public virtual void OnGUI_Specific(Rect position, SerializedProperty property, GUIContent label)
        {
            bool useFoldOut = property.hasVisibleChildren;
            position = OnGUI_FirstLine(position, property, label, useFoldOut);
            if(property.isExpanded)
                EditorGUI.PropertyField(position, property, useFoldOut);
        }


        protected bool IsAssigned(SerializedProperty property)
            => property.propertyType == SerializedPropertyType.ManagedReference && !string.IsNullOrEmpty(property.managedReferenceFullTypename);

    }
}