﻿using UnityEngine;
using UnityEditor;

using System;

namespace SixParQuatre
{
    public abstract class SerializeReferenceListDrawer : PropertyDrawer
    {
        static string listFieldName = "List";
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            SerializedProperty prop = property.FindPropertyRelative(listFieldName);
            if (prop == null)
                return EditorGUIUtility.singleLineHeight * 2;

            float height = EditorGUI.GetPropertyHeight(prop, label);
#if !UNITY_2020_1_OR_NEWER
        if (prop.isExpanded)
            height += EditorGUIUtility.singleLineHeight;
#endif
            return height;
        }

        protected abstract Type GetListType();

        static Type currentListType = null;
#if !UNITY_2020_1_OR_NEWER
    static SerializedProperty currentListProp = null;
    public static SerializedProperty CurrentListProp => currentListProp;
#endif
        public static Type CurrentListType => currentListType;
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty list = property.FindPropertyRelative(listFieldName);
            if (list == null)
            {
                position.height = EditorGUIUtility.singleLineHeight;
                //we need to do this because PrefixLabel doesn't work for some reason
                EditorGUI.LabelField(position, label);
                position = EditorGUI.PrefixLabel(position, label);
                position.height = 2 * EditorGUIUtility.singleLineHeight;
                EditorGUI.HelpBox(position, $"In this object script, please initialize this variable with new {GetListType().Name}(); then reload the scene/prefab.", MessageType.Error);
                return;
            }

            //Pray that the EditorGUI isn't multithreaded
            currentListType = GetListType();
#if !UNITY_2020_1_OR_NEWER
        currentListProp = list;
#endif
            position.x += 10;
            position.width -= 10;
            EditorGUI.PropertyField(position, list, label, true);
            currentListType = null;
#if !UNITY_2020_1_OR_NEWER
        currentListProp = null;
#endif
            if (!list.isExpanded)
                return;

            SerializeReferenceUtils.PopupType(position, list, GetListType());
        }

        static float grabIconWidth = 28;
        static public float GrabIconWidth => grabIconWidth;

        /// <summary>
        /// Print the index on the left margin of the array, before the grabIcon
        /// </summary>
        /// <param name="position"></param>
        /// <param name="property"></param>
        public static void IndexLabel(Rect position, SerializedProperty property)
        {
            if (!SerializeReferenceUtils.GetIndexOf(property, out var index))
                return;
            Rect posIndex = position;
            float w = GUI.skin.label.CalcSize(new GUIContent(index)).x;
            posIndex.x -= (grabIconWidth + w + EditorGUIUtility.standardVerticalSpacing);
            posIndex.x = Mathf.Max(posIndex.x, 0);
            posIndex.width = w;
            GUI.Label(posIndex, index);
        }
    }
}