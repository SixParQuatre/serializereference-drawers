using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
namespace SixParQuatre
{
    public static class SerializeReferenceUtils
    {
        /// <summary>
        /// Width of the +|- widget at the bottom of reorderable array GUI
        /// </summary>
#if UNITY_2020_1_OR_NEWER
        static float PlusMinusBumpWidth = 70;
#else
    static float PlusMinusBumpWidth = 0;
#endif

        static Dictionary<Type, List<Type>> ValidSubtypes = new Dictionary<Type, List<Type>>();
        static Dictionary<Type, GUIContent[]> ValidSubtypesContent = new Dictionary<Type, GUIContent[]>();

        /// <summary>
        /// Display a pop-up with all the valid subtype of rootType.
        /// Upon selecting one, create an instance of it and assign it to 
        /// property.managedReferenceValue or as a new element if the property is an array
        /// </summary>
        /// <param name="position"></param>
        /// <param name="property"></param>
        /// <param name="rootType"></param>
        public static void PopupType(Rect position, SerializedProperty property, Type rootType)
        {
            if (rootType == null)
                return;

            //1. Create a rect slightly to the left of the + - button at the end of the list.
            Rect postButton = new Rect(position);

            //2. Create the list of compatible types
            if (!ValidSubtypes.ContainsKey(rootType))
                CreateSubtypesInfo(rootType);

            GUIContent[] options = ValidSubtypesContent[rootType];

            //3. Move to the bottom of the rect if it's an array
            if (property.isArray)
            {
                postButton.y += postButton.height - EditorGUIUtility.singleLineHeight;
#if UNITY_2020_1_OR_NEWER
                postButton.y -= EditorGUIUtility.standardVerticalSpacing;
#else
            postButton.y += EditorGUIUtility.standardVerticalSpacing;
#endif
                postButton.height = EditorGUIUtility.singleLineHeight;
            }

            //4. Handle the case in which no classes were found (1 is because of the select/add... entry)
            if (options.Length == 1)
            {
                bool prevRichText = GUI.skin.label.richText;
                EditorStyles.label.richText = true;

                float width = position.width;
                string msg = $"No valid classes for <b>{rootType.Name}</b>";
                string tooltip = $"'{rootType.Name}' is probably an abstract class with no concrete child classes";

                GUIContent icon = EditorGUIUtility.IconContent("console.erroricon.sml");
                GUIContent msgContent = new GUIContent(msg, tooltip);

                if (property.isArray)
                    postButton.x += postButton.width - PlusMinusBumpWidth - 20 - EditorStyles.label.CalcSize(msgContent).x - 5;

                postButton.width = 20;
                icon.tooltip = tooltip;
                EditorGUI.LabelField(postButton, icon);
                postButton.x += postButton.width;
                postButton.width = width - postButton.width;

                EditorGUI.LabelField(postButton, msgContent);

                EditorStyles.label.richText = prevRichText;
                return;
            }

            //5. Adjust position and content depending on case
            if (property.isArray)
            {
                options[0].text = "Add...";
                float buttonWidth = EditorStyles.popup.CalcSize(options[0]).x;
                //offset to the right of the +|- widget
                postButton.x += postButton.width - PlusMinusBumpWidth - buttonWidth;
#if UNITY_2020_1_OR_NEWER
                postButton.x += EditorGUIUtility.standardVerticalSpacing;
#endif
                postButton.width = buttonWidth;
            }
            else
                options[0].text = "(select)";

            //6. pop up list
            int selection = EditorGUI.Popup(postButton, 0, options);
            if (selection != 0)
            {

                Type t = ValidSubtypes[rootType][selection - 1];
                object instance = Activator.CreateInstance(t);
                if (instance != null)
                {
                    if (property.isArray)
                    {
                        property.arraySize++;
                        property.GetArrayElementAtIndex(property.arraySize - 1).managedReferenceValue = instance;
                    }
                    else
                        property.managedReferenceValue = instance;
                }
            }
        }


        /// <summary>
        /// Create the valid subtype list and the associated GUIContent list with their names
        /// </summary>
        /// <param name="rootType"></param>
        static void CreateSubtypesInfo(Type rootType)
        {
            //1. Add the root type if it isn't abstract
            List<Type> validSubTypes = new List<Type>();
            if (!rootType.IsAbstract)
                validSubTypes.Add(rootType);

            //2. Add a derived type if it isn't abstract
            TypeCache.TypeCollection types = TypeCache.GetTypesDerivedFrom(rootType);
            foreach (Type t in types)
            {
                if (!t.IsAbstract) //We do not want abstract classes
                    validSubTypes.Add(t);
            }
            ValidSubtypes.Add(rootType, validSubTypes);

            //3. Create the GUIContent list for the pop-up
            List<GUIContent> content = new List<GUIContent>();
            content.Add(new GUIContent("")); //this will change from(select) to Add... depending on the context
            foreach (Type type in ValidSubtypes[rootType])
                content.Add(new GUIContent(type.Name));

            ValidSubtypesContent.Add(rootType, content.ToArray());
        }


        public static float clearButtonWidth = 20;
        /// <summary>
        /// Shows a button that sets property.managedReferenceValue = null at the end of the position rect
        /// and reduce the position.width as a result
        /// </summary>
        /// <param name="position"></param>
        /// <param name="property"></param>
        /// <returns></returns>
        public static bool ClearPropertyButton(ref Rect position, SerializedProperty property)
        {
            position.width -= clearButtonWidth;
            Rect posButton = new Rect(position);
            posButton.x += posButton.width;
            posButton.width = clearButtonWidth;
            posButton.height = EditorGUIUtility.singleLineHeight;

            string iconStr = "Toolbar Minus";
#if UNITY_2020_1_OR_NEWER
            iconStr = "Grid.EraserTool@2x";
#endif
            GUIContent icon = EditorGUIUtility.IconContent(iconStr);
            icon.tooltip = "Clear/Nullify";

            //Do a button and a label to make the icon more visible
            bool clicked = GUI.Button(posButton, "");
            GUI.Label(posButton, icon);

            if (clicked)
            {
                property.managedReferenceValue = null;
#if !UNITY_2020_1_OR_NEWER
            if (GetIndexOf(property, out var indexStr))
            {
                int i = int.Parse(indexStr);
                SerializeReferenceListDrawer.CurrentListProp.DeleteArrayElementAtIndex(i);
            }
#endif

            }
            return clicked;
        }

        public static bool IsInArray(SerializedProperty property)
                => property.propertyPath.EndsWith("]");

        public static bool GetIndexOf(SerializedProperty property, out string index)
        {
            index = null;
            string propPath = property.propertyPath;
            if (!propPath.EndsWith("]"))
                return false;

            int ind = propPath.LastIndexOf("[") + 1;
            index = propPath.Substring(ind, propPath.Length - 1 - ind);
            return true;
        }
    }
}
