using UnityEditor;
using System.IO;
using System;

namespace SixParQuatre
{
    //Some of this code is directly grabbed from LotteMakeStuff CustomInspectorCreator
    //https://gist.github.com/LotteMakesStuff/cb63e4e25e5dfdda19a95380e9c03436
    public class SerializeReferenceDrawersCreator
    {
        const string toolName = "SerializeReference Drawers";
        const string toolPath = "Assets/Create/C# Advanced/" + toolName;
        [MenuItem(toolPath, true, priority = 81)]
        static bool IsSelectionCompatible()
        {
            if (Selection.objects.Length != 1)
                return false;

            MonoScript monoScript = Selection.objects[0] as MonoScript;
            if (monoScript == null)
                return false;

            Type type = monoScript.GetClass();
            if (type.IsAbstract && type.IsSealed) //check if it's static
                return false;

            bool hasConcreteSubclasses = false;
            foreach (Type subType in TypeCache.GetTypesDerivedFrom(type))
            {
                if (!subType.IsAbstract)
                {
                    hasConcreteSubclasses = true;
                    break;
                }
            }
            
            if(!hasConcreteSubclasses && type.IsAbstract)
                return false;

            return !typeof(UnityEngine.Object).IsAssignableFrom(type);
        }



        static string listTemplatePath = null;
        static string listDrawerTemplatePath = null;
        static string classDrawerTemplatePath = null;

        [MenuItem(toolPath, priority = 81)]
        static void CreateScriptAndDrawer()
        {
            MonoScript monoScript = Selection.objects[0] as MonoScript;
            string assetPath = AssetDatabase.GetAssetPath(monoScript);
            var className = Path.GetFileNameWithoutExtension(assetPath);
            string scriptNamespace = monoScript.GetClass().Namespace;
            string typeName = $"{className}List";

            RefreshTemplates();

            string script = File.ReadAllText(listTemplatePath);
            string listDrawer = File.ReadAllText(listDrawerTemplatePath);
            string typeDrawer = File.ReadAllText(classDrawerTemplatePath);

            Customize(ref script, className, scriptNamespace);
            Customize(ref listDrawer, className, scriptNamespace);
            Customize(ref typeDrawer, className, scriptNamespace);


            //get current context 
            string scriptFolder = Path.GetDirectoryName(assetPath);
            string drawerFolder = scriptFolder + "/Editor";

            // make sure a editor folder exists for us to put this script into...
            if (!Directory.Exists(drawerFolder))
                Directory.CreateDirectory(drawerFolder);

            string scriptPath = scriptFolder + $"/{typeName}.cs";
            string listDrawerPath = drawerFolder + $"/{typeName}Drawer.cs";
            string drawerPath = drawerFolder + $"/{className}Drawer.cs";

            if (File.Exists(scriptPath) || File.Exists(listDrawerPath) || File.Exists(drawerPath))
            {
                EditorUtility.DisplayDialog("Create " + toolName, $"{scriptPath}\n\nor\n\n{listDrawerPath}\n\nor\n\n{drawerPath}\nalready exist", "OK");
                return;
            }

            File.WriteAllText(scriptPath, script);
            File.WriteAllText(listDrawerPath, listDrawer);
            File.WriteAllText(drawerPath, typeDrawer);

            AssetDatabase.Refresh();

        }

        static void Customize(ref string script, string className, string scriptNamespace)
        {
            ReplaceParam(ref script, "CLASS_NAME", className);
            if (!string.IsNullOrEmpty(scriptNamespace))
                ReplaceParam(ref script, "NAMESPACE", scriptNamespace);
            else
                RemoveNamespace(ref script);
        }

        #region Template Utilities


        static void ReplaceParam(ref string script, string paramName, string value)
           => script = script.Replace("[{" + paramName + "}]", value);

        static void RemoveNamespace(ref string script)
        {
            script = script.Replace("\nnamespace [{NAMESPACE}]", "");
            script = script.Remove(script.IndexOf("{") - 1, 2);
            script = script.Remove(script.LastIndexOf("}") - 1, 2);
            script = script.Replace("\n    ", "\n");
            script = script.Replace("\n\t", "\n");

        }

        static void RefreshTemplates()
        {
            if (listTemplatePath == null)
                listTemplatePath = FindPathForTemplate("List");

            if (listDrawerTemplatePath == null)
                listDrawerTemplatePath = FindPathForTemplate("ListDrawer");


            if (classDrawerTemplatePath == null)
                classDrawerTemplatePath = FindPathForTemplate("ClassDrawer");
        }

        /// <summary>
        /// Function that allows to find  a cstemplate by name no matter if it's a project, a custom package, whatevs
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        static string FindPathForTemplate(string name)
        {
            string filename = name + ".cstemplate";
            foreach (string guid in AssetDatabase.FindAssets($"t:DefaultAsset {name}"))
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                if (path.EndsWith(filename))
                    return path;
            }
            return null;
        }
        #endregion
    }
}