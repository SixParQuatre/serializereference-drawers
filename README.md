# SerializeReference Drawers

> A unity package that adds better support of polymorphic types and \[SerializeReference\] in the inspector, including in lists.

The screenshots below are from **2020.3**; but the package has been tested with **2019.4**, **2021.3.15**, **2022.2.0b16** and **2023.1.0a21**, albeit with a slightly different UX since the list didn't use the ReorderableList then.

![image](https://gitlab.com/SixParQuatre/serializereference-drawers/-/wikis/uploads/fb76cb7cbc8ad557f34295618dceaa6b/image.png)

![Untitled_3](https://gitlab.com/SixParQuatre/serializereference-drawers/-/wikis/uploads/63a834d6b6de1b31f17140a2181ac8ed/Untitled_3.png)
# How to use it

## Using the [ClassPicker] attribute
Replace **\[SerializeReference\]** with **\[SerializeReference, ClassPicker\]** and you will see the drop down list.

:warning:️ On Collections (arrays, list), this will not add the drop-down menu to add an entry of a chosen type; you'll have to add an empty entry with + and choose the type.

## Creating specific drawers for the class and list.

:bulb: For the following, we'll assume you clicked on a file/class named **BaseClass**

**Right-click on the MonoScript** in which your most generic class is defined, then use **Create→C# Advanced→SerializeReference List**

This will create 3 new files:

- **BaseClassList.cs**: as the runtime class for encapsulating the list
- **Editor/BaseClassListDrawer.cs:** a drawer for the list
- **Editor/BaseClassDrawer.cs**: a drawer for single variable of this type

You can now declare

- **BaseClassList** variable in other classes, and you will find the **Add...** pop-up list at the end; allowing you to add an instance of any compatible types

:warning:️ Don't forget to initialize your variable with **new BaseClassList()**. Otherwise the drawer will be confused

- **BaseClass or any of its subtypes** variables in other class and a drawer will let you choose which subtype to use, and a button to nullify it and be able to reselect.

:bulb: If you want to use specific Drawer for any subclass of your parent class, make a PropertyDrawer that inherits from **BaseClassDrawer** and override `public override void OnGUI_Specific(Rect position, SerializedProperty property, GUIContent label)`

# How it works

## The Variable Drawer

There's an underlying **SerializeReferenceDrawer which a PropertyDrawer**, very  generic and has 4 responsibilities:

1. **displaying a select a pop-up menu** when the property is unassigned

   ![Untitled_4](https://gitlab.com/SixParQuatre/serializereference-drawers/-/wikis/uploads/cbdb31a5fa210b3af16aba967c893efb/Untitled_4.png)
2. **displaying a clear button** when a property not in an array is assigned (so that user can select again)

   ![Untitled_5](https://gitlab.com/SixParQuatre/serializereference-drawers/-/wikis/uploads/a96d5988d89435e584a7d59a2e999814/Untitled_5.png)
3. **displaying errors** if the type used for a variable has no valid classes to select from.

   ![Untitled_6](https://gitlab.com/SixParQuatre/serializereference-drawers/-/wikis/uploads/2c82ade14c2b5eaff6ec3f017bb9799e/Untitled_6.png)

**Create→C# Advanced→SerializeReference List** simply creates a specific subclass of **SerializeReferenceDrawer** and assign it to the selected class.

## The List Drawer

There's an underlying **SerializeReferenceList** which contains a public List and functions exposing most of the List function and property that I use.

There is a base PropertyDrawer **SerializeReferenceListDrawer**, which display the pop-up list at the bottom, and if any null entry is found, in the array entry as well.

**Create→C# Advanced→SerializeReference List** simply creates a specific subclass of **SerializeReferenceList** with T as the selected class; as well as a specific drawer for that class which inherits from **SerializeReferenceListDrawer**

# Limitations

- **Doesn't hook itself to the +.** I couldn't find how to do that.
- **Doesn't hide the +.** I tried putting the pop-up over it, but the click on the + was still registered.
- **It creates unnecessary classes.** Sadly, I couldn't find how to make a drawer attribute work on collections(List/Array) alongside; hence why new classes and drawers associated with them are necessary. Upside is that you won't have to remember to add an attribute :slight_smile:
- **If you change the class abstractions, Unity will be confused** and will still keep instances of class that were concrete and are now abstract.

:fire: A native implementation by Unity would probably get rid of all of that...
